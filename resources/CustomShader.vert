layout(location = 0) in vec4 position;
layout(location = 1) in vec3 color;
layout(location = 2) in vec3 normal;

uniform mat4 transformation;
uniform mat4 projection;
uniform mat3 normalMatrix;
uniform vec3 eyePos;
uniform mat4 viewMatrix;

out vec3 interpolatedColor;
out vec3 transformedNormal;
out vec3 cameraDirection;
out vec3 transformedPosition;
out mat4 transformationMatrix;

void main() {
    vec4 transformedPosition4 = transformation*position;
    transformedPosition = transformedPosition4.xyz/transformedPosition4.w;
    gl_Position = projection*transformedPosition4;

    cameraDirection = -transformedPosition;

    interpolatedColor = color;
    transformedNormal = normalMatrix*normal;

    transformationMatrix = viewMatrix;
}
