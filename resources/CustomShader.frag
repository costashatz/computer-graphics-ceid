in vec3 interpolatedColor;
in vec3 transformedNormal;
in vec3 cameraDirection;
in vec3 transformedPosition;
in mat4 transformationMatrix;

out vec4 fragmentColor;

struct lightSource
{
    vec4 position;
    vec4 diffuse;
    vec4 specular;
    float constantAttenuation, linearAttenuation, quadraticAttenuation;
    float spotCutoff, spotExponent;
    vec3 spotDirection;
};

const int MAX_LIGHTS = 8;

uniform lightSource[MAX_LIGHTS] lights;
uniform int numberOfLights = 0;

void main() {
    vec3 total = vec3(0,0,0);
    vec3 normalDir = normalize(transformedNormal);
    vec3 viewDir = normalize(cameraDirection);

    vec3 lightDirection;
    float attenuation;

    int numOfLights = min(numberOfLights,MAX_LIGHTS);

    for (int index = 0; index < numOfLights; index++) // for all light sources
    {
        vec4 lightpos = transformationMatrix*lights[index].position;
        if (0.0 == lights[index].position.w) // directional light?
        {
            attenuation = 1.0; // no attenuation
            lightDirection = normalize(vec3(lightpos));
        }
        else
        {
            lightDirection = vec3(lightpos)-transformedPosition;
            float distance = length(lightDirection);
            lightDirection = normalize(lightDirection);
            attenuation = 1.0 / (lights[index].constantAttenuation
                                 + lights[index].linearAttenuation * distance
                                 + lights[index].quadraticAttenuation * distance * distance);
            if (lights[index].spotCutoff <= 90.0) // spotlight?
            {
                float clampedCosine = max(0.0, dot(-lightDirection, normalize(lights[index].spotDirection)));
                if (clampedCosine < cos(radians(lights[index].spotCutoff))) // outside of spotlight cone?
                {
                    attenuation = 0.0;
                }
                else
                {
                    attenuation = attenuation * pow(clampedCosine, lights[index].spotExponent);
                }
            }
        }

        vec3 diffuseReflection = attenuation
                * vec3(lights[index].diffuse) * vec3(interpolatedColor)
                * max(0.0, dot(normalDir, lightDirection));

        vec3 specularReflection;
        if (dot(normalDir, lightDirection) < 0.0) // light source on the wrong side?
        {
            specularReflection = vec3(0.0, 0.0, 0.0); // no specular reflection
        }
        else // light source on the right side
        {
            specularReflection = attenuation * vec3(lights[index].specular) * interpolatedColor
                    * pow(max(0.0, dot(reflect(-lightDirection, normalDir), viewDir)), 1.0);
        }

        total = total + diffuseReflection + specularReflection;
    }
    fragmentColor.rgb = total+vec3(0.05,0.05,0.05)*interpolatedColor; //add ambient term
    fragmentColor.a = 1.0;
    if(numberOfLights==0)
        fragmentColor.rgb = interpolatedColor;
}
