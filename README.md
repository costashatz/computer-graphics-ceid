###Computer Graphics Project for a University course

#####Chatzilygeroudis Konstantinos

####Dependencies

1. C++11 3D Graphics Engine Magnum ([link](https://github.com/mosra/magnum))
2. GLUT (freeglut)

####How to use:

######To compile/build:

```bash
mkdir build && cd build
cmake ..
make
```

######To run (assuming in root directory):

```bash
./build/src/MyApplication
```
