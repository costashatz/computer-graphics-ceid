#include "simpleobject.h"

// Algorithms
#include <Magnum/Math/Algorithms/GramSchmidt.h>

SimpleObject::SimpleObject(string n, const ResourceKey meshKey, const ResourceKey shaderKey, Object3D* parent, SceneGraph::DrawableGroup3D* group)
    : SceneObject(n,meshKey,{0,0,0},{0,0,0},parent,group),
      shader(CgResourceManager::instance().get<CustomShader>(shaderKey))
{
    shape = NULL;
}

void SimpleObject::update(Float duration)
{
    // Cube does nothing
}

void SimpleObject::setShader(const ResourceKey shaderKey)
{
    shader = CgResourceManager::instance().get<CustomShader>(shaderKey);
}

void SimpleObject::setShape(Shapes::AbstractShape3D *sh)
{
    shape = (Shapes::Shape<Shapes::AxisAlignedBox3D>*)sh;
}

Shapes::AbstractShape3D* SimpleObject::getShape()
{
    return shape;
}

void SimpleObject::draw(const Matrix4& transformationMatrix, SceneGraph::AbstractCamera3D& camera)
{
    shader->setTransformationMatrix(transformationMatrix)
            .setProjectionMatrix(camera.projectionMatrix())
            .setNormalMatrix(Math::Algorithms::gramSchmidtOrthonormalize(transformationMatrix.rotationScaling()))
            .use();
    mesh->draw(*shader);
}
