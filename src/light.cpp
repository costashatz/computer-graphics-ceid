#include "light.h"

Light::Light(Object3D* parent, Vector4 pos, Color4 diff, Color4 spec, Float cAtt, Float lAtt, Float qAtt, Float spCutoff, Float spotExp, Vector3 spotDir)
    :Object3D(parent),
      position(pos),
      diffuse(diff),
      specular(spec),
      constantAttenuation(cAtt),
      linearAttenuation(lAtt),
      quadraticAttenuation(qAtt),
      spotCutoff(spCutoff),
      spotExponent(spotExp),
      spotDirection(spotDir)
{
    translate(pos.xyz());
}
