// Magnum Required
#include <Magnum/DefaultFramebuffer.h>
#include <Magnum/Platform/GlutApplication.h>

// Resource Manager
#include <Magnum/ResourceManager.h>

// Renderer
#include <Magnum/Renderer.h>
#include <Magnum/Color.h>

// Mesh
#include <Magnum/Mesh.h>

// Mesh Tools
#include <Magnum/MeshTools/Interleave.h>
#include <Magnum/MeshTools/CompressIndices.h>
#include <Magnum/MeshTools/GenerateFlatNormals.h>
#include <Magnum/MeshTools/CombineIndexedArrays.h>
#include <Magnum/MeshTools/FlipNormals.h>

// Trade
#include <Magnum/Trade/MeshData3D.h>
#include <Magnum/Trade/ImageData.h>
// Plugin Importer
#include <Magnum/Trade/AbstractImporter.h>
#include <Corrade/PluginManager/Manager.h>

// Primitives
#include <Magnum/Primitives/UVSphere.h>

// Scene Graph
#include <Magnum/SceneGraph/Camera3D.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Scene.h>

// Shapes (for collision)
#include <Magnum/Shapes/Shape.h>
#include <Magnum/Shapes/ShapeGroup.h>
#include <Magnum/Shapes/Sphere.h>
#include <Magnum/Shapes/Collision.h>
#include <Magnum/Shapes/AbstractShape.h>
#include <Magnum/Shapes/AxisAlignedBox.h>

// Texture
#include <Magnum/Texture.h>
#include <Magnum/TextureFormat.h>

// Magnum Timer
#include <Magnum/Timeline.h>

// Magnum Math
#include <Magnum/Math/Constants.h>
#include <Magnum/Math/Angle.h>

// Custom Shaders
#include "customshader.h"
#include "sphereshader.h"

// Simple Objects
#include "simpleobject.h"
#include "sphereobject.h"

// Light
#include "light.h"

// std includes
#include <algorithm>
#include <random>
#include <functional>

// Include file needed for Importers compilation
#include "configure.h"

// Typedefs
#include "typedefs.h"

// Std headers
#include <memory>

using namespace Magnum;
using namespace std;

bool sphere_plane(Vector3 p, Float r, Vector3 left, Vector3 right, Vector3 top, Vector3 ltop, Vector3& point, Vector3& n)
{
    auto normal = Vector3();
    normal = (left+right+top+ltop).normalized();
    auto pp = (left+right+top+ltop)/4.0f;
    auto v1 = p-pp;
    Float b = fabs(Vector3::dot(normal,v1));
    if(b<=r)
    {
        auto Q = p-b*normal;
        auto radius = sqrt(pow(r,2.0)+pow(b,2.0));
        point = Q;
        n = normal;
        return true;
    }
    return false;
}

class MyApplication: public Platform::Application {
public:
    explicit MyApplication(const Arguments& arguments);

private:
    // Member variables
    CgResourceManager resourceManager;
    Scene3D scene;
    Object3D *o, *cameraObject;
    SceneGraph::Camera3D* camera;
    SceneGraph::DrawableGroup3D* drawables;
    Timeline timeline;
    CustomShader* shader;
    SphereShader* sphere_shader;
    vector<std::unique_ptr<Light> > lights;
    Shapes::ShapeGroup3D* collision_shapes;
    Texture2D* sphereTexture;
    bool areSpheresTextured;


    void drawEvent() override;
    void keyPressEvent(KeyEvent& event) override;
    void viewportEvent(const Vector2i& size) override;
    void updateLoop(Float duration);
    void createCubeBuffers();
    void createSphereBuffers();
};

MyApplication::MyApplication(const Arguments& arguments): Platform::Application(arguments,nullptr) {
    //Set title and size
    createContext(Configuration().setTitle("Computer Graphics Project").setSize({1024, 768}));
    // Initialize timer
    timeline.setMinimalFrameTime(1/120.0f); //120 FPS at max
    // Initialize renderer
    Renderer::setFeature(Renderer::Feature::DepthTest, true);
    Renderer::setFeature(Renderer::Feature::FaceCulling, true);

    // Create drawable group
    drawables = new SceneGraph::DrawableGroup3D();

    // Create collision group
    collision_shapes = new Shapes::ShapeGroup3D();

    // Initialize shaders
    shader = new CustomShader();
    sphere_shader = new SphereShader();
    // Store shaders into resource manager for later usage
    resourceManager.set("shader",shader);
    resourceManager.set("sphere_shader",sphere_shader);

    // Load image and Create Texture
    // Load the tga importer plugin
    PluginManager::Manager<Trade::AbstractImporter> manager(MAGNUM_PLUGINS_IMPORTER_DIR);
    if(!(manager.load("TgaImporter") & PluginManager::LoadState::Loaded))
    {
        Error()<<"Cannot load importer";
        std::exit(1);
    }
    std::unique_ptr<Trade::AbstractImporter> importer = manager.instance("TgaImporter");
    // Load the actual image
    Utility::Resource rs("data");
    if(!importer->openData(rs.getRaw("texture.tga"))) {
        Error() << "Cannot load texture";
        std::exit(2);
    }
    // Create Texture
    std::optional<Trade::ImageData2D> image = importer->image2D(0);
    CORRADE_INTERNAL_ASSERT(image);
    sphereTexture = new Texture2D();
    sphereTexture->setWrapping(Sampler::Wrapping::ClampToEdge)
            .setMagnificationFilter(Sampler::Filter::Linear)
            .setMinificationFilter(Sampler::Filter::Linear)
            .setImage(0, TextureFormat::RGB8, *image);

    // Create camera and place it at (0,0,-3) looking at (0,0,0)
    // Object attached to camera
    (cameraObject = new Object3D(&scene))->translate(Vector3::zAxis(3.0f));
    // Actual camera
    (camera = new SceneGraph::Camera3D(*cameraObject))->setAspectRatioPolicy(SceneGraph::AspectRatioPolicy::Extend)
            .setPerspective(Deg(65.0f), 1.f, 0.001f, 100.0f)
            .setViewport(defaultFramebuffer.viewport().size());

    // Default object, parent of all (for manipulation)
    o = new Object3D(&scene);

    // Create cube buffers and store them into the resource manager
    createCubeBuffers();
    // Create sphere buffers and store them into the resource manager
    createSphereBuffers();

    // Create cube object
    new SimpleObject("cube_outer","cube","shader",o,drawables);
    new SimpleObject("cube_inner","cube2","shader",o,drawables);

    // Spheres are not textured at first
    areSpheresTextured = false;

    // Set clear color
    Renderer::setClearColor({0.2f, 0.2f, 0.2f});
    // Start timer
    timeline.start();
}

void MyApplication::drawEvent() {
    // Clear color and depth
    defaultFramebuffer.clear(FramebufferClear::Color|FramebufferClear::Depth);

    // Duration of frame
    Float duration = timeline.previousFrameDuration();

    // Update scene/objects
    updateLoop(duration);

    // Update shader uniform variables
    sphere_shader->setViewMatrix(cameraObject->transformationMatrix().inverted());
    shader->setViewMatrix(cameraObject->transformationMatrix().inverted());

    // Draw objects
    camera->draw(*drawables);

    // Swap buffers (using double buffer)
    swapBuffers();
    // Instant redraw
    redraw();
    // Timer go to next frame
    timeline.nextFrame();
}

void MyApplication::keyPressEvent(KeyEvent& event)
{
    // Static variables for random generation and sphere creation management
    static int num=0, toRemove=0;
    static std::random_device rd;
    static std::default_random_engine e1(rd());
    static std::uniform_real_distribution<float> uniform_dist(-0.9f, 0.9f), vel_dist(0.5f,2.0f), col_dist(0.0f,1.0f);
    static std::uniform_int_distribution<int> int_dist(0,4);
    // Temp variables
    auto toAdd = std::to_string(num);
    auto toRem = std::to_string(toRemove);
    float x1,y1,z1,vx1,vy1,vz1,r,g,b;
    Vector3 vec, vel;
    SphereObject* tmp;
    // Light Management
    static int LIGHTING_SCHEME = 0;
    static int LIGHTS_ON = 0;
    static Color4 diff{1.f,1.f,1.f,1.f}, spec{1.f,1.f,1.f,1.f};
    static Float cAtt=0.2, lAtt=0.2, qAtt=0;
    static Float sc = 100.f, se = 0;
    static Vector3 sd{0.f,0.f,0.f};
    static Float w = 1.0f;
    // Key Events
    switch(event.key())
    {
    // Camera Movement
    case KeyEvent::Key::W:
    case KeyEvent::Key::Up:
        cameraObject->translate(Vector3::zAxis(-0.1f));
        break;
    case KeyEvent::Key::S:
    case KeyEvent::Key::Down:
        cameraObject->translate(Vector3::zAxis(0.1f));
        break;
    case KeyEvent::Key::A:
    case KeyEvent::Key::Left:
        cameraObject->translate(Vector3::xAxis(-0.1f));
        break;
    case KeyEvent::Key::D:
    case KeyEvent::Key::Right:
        cameraObject->translate(Vector3::xAxis(0.1f));
        break;
    case KeyEvent::Key::Z:
        cameraObject->translate(Vector3::yAxis(0.1f));
        break;
    case KeyEvent::Key::X:
        cameraObject->translate(Vector3::yAxis(-0.1f));
        break;
    // Add/Delete sphere
    case KeyEvent::Key::Plus:
        x1 = uniform_dist(e1);
        y1 = uniform_dist(e1);
        z1 = uniform_dist(e1);
        vec = {x1,y1,z1};
        vx1 = ((int_dist(e1)>2)?-1:1)*vel_dist(e1);
        vy1 = ((int_dist(e1)>2)?-1:1)*vel_dist(e1);
        vz1 = ((int_dist(e1)>2)?-1:1)*vel_dist(e1);
        vel = {vx1,vy1,vz1};
        r = col_dist(e1);
        g = col_dist(e1);
        b = col_dist(e1);
        tmp = new SphereObject("sphere"+toAdd,"sphere","sphere_shader",vel,{r,g,b},o,drawables);
        tmp->scale(Vector3(0.05f)).translate(vec);
        tmp->setShape(new Shapes::Shape<Shapes::Sphere3D>(*tmp,{{},1.0f},collision_shapes));
        tmp->setTexture(sphereTexture);
        tmp->setTextured(areSpheresTextured);
        num++;
        break;
    case KeyEvent::Key::Minus:
        for(auto child = o->firstChild(); child; child = child->nextSibling())
        {
            if(string(typeid(*child).name()).find("Object")==string::npos)
                continue;
            if(((SphereObject*)child)->Name()==("sphere"+toRem))
            {
                auto s = ((SphereObject*)child)->getShape();
                collision_shapes->remove(*s);
                delete s;
                delete child;
                toRemove++;
            }
        }
        break;
    // Toggle Texturing
    case KeyEvent::Key::T:
        for(auto child = o->firstChild(); child; child = child->nextSibling())
        {
            if(string(typeid(*child).name()).find("Sphere")==string::npos)
                continue;
            auto c = (SphereObject*)child;
            c->setTextured(!areSpheresTextured);
        }
        areSpheresTextured = !areSpheresTextured;
        break;
    // Change Lighting Schemes
    case KeyEvent::Key::P:
        if(!LIGHTS_ON)
            break;
        if(LIGHTING_SCHEME==0 || LIGHTING_SCHEME>=3)
        {
            lights.clear();
            lights.push_back(std::unique_ptr<Light>(new Light(o,{0.f,0.f,0.f,w},diff,spec,cAtt,lAtt,qAtt,sc,se,sd)));
            LIGHTING_SCHEME=1;
        }
        else if(LIGHTING_SCHEME==1)
        {
            lights.clear();
            lights.push_back(std::unique_ptr<Light>(new Light(o,{0.8f,0.8f,0.8f,w},diff,spec,cAtt,lAtt,qAtt,sc,se,sd)));
            LIGHTING_SCHEME=2;
        }
        else if(LIGHTING_SCHEME==2)
        {
            lights.clear();
            lights.push_back(std::unique_ptr<Light>(new Light(o,{-0.8f,-0.8f,0.8f,w},diff,spec,cAtt,lAtt,qAtt,sc,se,sd)));
            LIGHTING_SCHEME=3;
        }
        sphere_shader->setLights(lights);
        sphere_shader->setNumberOfLights(lights.size());
        shader->setLights(lights);
        shader->setNumberOfLights(lights.size());
        break;
    // Enable/Disable Lighting
    case KeyEvent::Key::L:
        if(LIGHTS_ON==0)
        {
            // Turn Lights On to Point Lights (and switch to first lighting scheme)
            sc = 100.f, se = 0;
            sd = Vector3();
            w = 1.0f;
            lights.clear();
            lights.push_back(std::unique_ptr<Light>(new Light(o,{0.f,0.f,0.f,w},diff,spec,cAtt,lAtt,qAtt,sc,se,sd)));
            LIGHTING_SCHEME=1;
            LIGHTS_ON=1;
        }
        else if(LIGHTS_ON==1)
        {
            // Switch to Spotlights
            sc = 90.f, se = 0.8f;
            sd = Vector3(1.0f,1.0f,-1.0f);
            for(UnsignedInt i=0;i<lights.size();i++)
            {
                lights[i]->spotCutoff = sc;
                lights[i]->spotExponent = se;
                lights[i]->spotDirection = sd;
            }
            LIGHTS_ON=2;
        }
        else if(LIGHTS_ON==2)
        {
            // Switch to Directional Lights
            sc = 100.f, se = 0;
            sd = Vector3();
            w = 0.0f;
            for(UnsignedInt i=0;i<lights.size();i++)
            {
                lights[i]->position.w() = 0.0f;
            }
            LIGHTS_ON=3;
        }
        else if(LIGHTS_ON==3)
        {
            // Turn off lights
            lights.clear();
            LIGHTS_ON=0;
        }
        sphere_shader->setLights(lights);
        sphere_shader->setNumberOfLights(lights.size());
        shader->setLights(lights);
        shader->setNumberOfLights(lights.size());
        break;
    // Quit
    case KeyEvent::Key::Q:
        exit(0);
    default:
        break;
    }
}

void MyApplication::viewportEvent(const Vector2i& size)
{
    defaultFramebuffer.setViewport({{}, size});
}

void MyApplication::updateLoop(Float duration)
{
    // Check for collisions
    for(auto child = o->firstChild(); child; child = child->nextSibling())
    {
        if(string(typeid(*child).name()).find("Sphere")==string::npos)
            continue;
        Shapes::Shape<Shapes::Sphere3D>* shape = (Shapes::Shape<Shapes::Sphere3D>*)((SceneObject*)child)->getShape();
        if(!shape)
            continue;
        Shapes::AbstractShape3D* other = collision_shapes->firstCollision(*shape);
        if(!other)
            continue;
        Shapes::Collision3D col;
        if(string(typeid(other->object()).name()).find("Sphere")!=string::npos)
        {
            col = shape->transformedShape()/((Shapes::Shape<Shapes::Sphere3D>*)other)->transformedShape();
        }
        else
        {
            //engine does not support aabb and sphere collisions
            //I am implementing a simple way for sphere plane/quad collision (further down)
            continue;
        }
        if(col)
        {
            auto v = ((SceneObject*)child)->getVelocity();
            auto n = col.separationNormal();
            ((SceneObject*)child)->setVelocity(v-2*Vector3::dot(v,n)*n);
            ((SceneObject*)child)->translate(col.separationDistance()*col.separationNormal());
            if(string(typeid(other->object()).name()).find("Sphere")!=string::npos)
            {
                v = ((SceneObject*)(&other->object()))->getVelocity();
                n = -n;
                ((SceneObject*)(&other->object()))->setVelocity(v-2*Vector3::dot(v,n)*n);
            }
        }
    }
    // sphere/plane intersections - very basic/simple and un-efficient (simple reflection in velocity and small translation for not overlapping)
    for(auto child2 = o->firstChild(); child2; child2 = child2->nextSibling())
    {
        if(string(typeid(*child2).name()).find("Sphere")==string::npos)
            continue;
        auto c = (SphereObject*)child2;
        Vector3 p,n;
        // Check for every face of the cube (even that does not exist)
        if(sphere_plane(c->transformation().translation(),0.05f,{-1.0f,-1.0f,-1.0f},{-1.0f, -1.0f, 1.0f},{-1.0f, 1.0f, -1.0f},{-1.0f, 1.0f, 1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
        else if(sphere_plane(c->transformation().translation(),0.05f,{-1.0f,-1.0f,-1.0f},{-1.0f,1.0f,-1.0f},{1.0f,-1.0f,-1.0f},{1.0f, 1.0f, -1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
        else if(sphere_plane(c->transformation().translation(),0.05f,{1.0f,-1.0f,-1.0f},{1.0f,-1.0f,1.0f},{1.0f,1.0f,-1.0f},{1.0f, 1.0f, 1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
        else if(sphere_plane(c->transformation().translation(),0.05f,{-1.0f,1.0f,-1.0f},{-1.0f,1.0f,1.0f},{1.0f,1.0f,-1.0f},{1.0f, 1.0f, 1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
        else if(sphere_plane(c->transformation().translation(),0.05f,{-1.0f,-1.0f,1.0f},{1.0f,-1.0f,1.0f},{-1.0f,1.0f,1.0f},{1.0f, 1.0f, 1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
        else if(sphere_plane(c->transformation().translation(),0.05f,{-1.0f,-1.0f,-1.0f},{-1.0f,-1.0f,1.0f},{1.0f,-1.0f,-1.0f},{1.0f, -1.0f, 1.0f},p,n))
        {
            auto v = c->getVelocity();
            c->setVelocity(v-2*Vector3::dot(v,n)*n);
            c->translate(p-c->transformation().translation());
        }
    }
    // Update spheres' positions
    for(auto child = o->firstChild(); child; child = child->nextSibling())
    {
        if(string(typeid(*child).name()).find("Object")==string::npos)
            continue;
        if(((SphereObject*)child)->Name().find("sphere")==0)
        {
            ((SphereObject*)child)->update(duration);
        }
    }
}

void MyApplication::createCubeBuffers()
{
    // Cube Buffer creation (multiple colors)
    Mesh* mesh = new Mesh(), *mesh2 = new Mesh();
    Buffer* indexBuffer = new Buffer(), *vertexBuffer=new Buffer();

    std::vector<Vector3> data({{-1.0f, -1.0f, -1.0f},
                               {-1.0f, -1.0f, 1.0f},
                               {-1.0f, 1.0f, -1.0f},
                               {-1.0f, 1.0f, 1.0f},
                               {1.0f, -1.0f, -1.0f},
                               {1.0f, -1.0f, 1.0f},
                               {1.0f, 1.0f, -1.0f},
                               {1.0f, 1.0f, 1.0f}});

    std::vector<Vector3> colors({{1.0f, 0.0f, 0.0f},
                               {0.0f, 1.0f, 0.0f},
                               {0.0f, 0.0f, 1.0f},
                               {1.0f, 1.0f, 0.0f},
                               {0.0f, 1.0f, 1.0f} });


    std::vector<UnsignedInt> indices({
                                         0, 1, 3,
                                         6, 0, 2,
                                         5, 0, 4,
                                         6, 4, 0,
                                         0, 3, 2,
                                         5, 1, 0,
                                         //3, 1, 5, //don't want front face
                                         7, 4, 6,
                                         4, 7, 5,
                                         7, 6, 2,
                                         7, 2, 3,
                                         //7, 3, 5 //don't want front face
                                     });

    vector<UnsignedInt> normalIndices;
    vector<Vector3> normals;
    std::tie(normalIndices, normals) = MeshTools::generateFlatNormals(indices, data);
    indices = MeshTools::combineIndexedArrays(
                std::make_tuple(std::cref(indices), std::ref(data)),
                std::make_tuple(std::cref(normalIndices), std::ref(colors)),
                std::make_tuple(std::cref(normalIndices), std::ref(normals)));

    MeshTools::compressIndices(*mesh, *indexBuffer, BufferUsage::StaticDraw, indices);
    MeshTools::interleave(*mesh, *vertexBuffer, BufferUsage::StaticDraw, data, colors, normals);
    mesh->setPrimitive(MeshPrimitive::Triangles)
            .addVertexBuffer(*vertexBuffer, 0, CustomShader::Position(), CustomShader::Color(), CustomShader::Normal());

    resourceManager.set("cube_indices", indexBuffer);
    resourceManager.set("cube_vertices", vertexBuffer);
    resourceManager.set("cube", mesh);

    indexBuffer = new Buffer(), vertexBuffer=new Buffer();

    transform(data.begin(), data.end(), data.begin(),
              std::bind1st(std::multiplies<Vector3>(),0.99999f));

    MeshTools::flipNormals(indices, normals);

    MeshTools::compressIndices(*mesh2, *indexBuffer, BufferUsage::StaticDraw, indices);
    MeshTools::interleave(*mesh2, *vertexBuffer, BufferUsage::StaticDraw, data, colors, normals);
    mesh2->setPrimitive(MeshPrimitive::Triangles)
            .addVertexBuffer(*vertexBuffer, 0, CustomShader::Position(), CustomShader::Color(), CustomShader::Normal());

    resourceManager.set("cube2_indices", indexBuffer);
    resourceManager.set("cube2_vertices", vertexBuffer);
    resourceManager.set("cube2", mesh2);

}

void MyApplication::createSphereBuffers()
{
    // Sphere Buffer creation (green color)
    Buffer* indexBufferS=new Buffer(), *vertexBufferS=new Buffer();
    Mesh* meshS = new Mesh();
    Trade::MeshData3D sphere = Primitives::UVSphere::solid(50,100,Primitives::UVSphere::TextureCoords::Generate);
    MeshTools::compressIndices(*meshS, *indexBufferS, BufferUsage::StaticDraw, sphere.indices());
    MeshTools::interleave(*meshS, *vertexBufferS, BufferUsage::StaticDraw, sphere.positions(0), sphere.normals(0), sphere.textureCoords2D(0));
    meshS->setPrimitive(sphere.primitive())
            .addVertexBuffer(*vertexBufferS, 0, SphereShader::Position(), SphereShader::Normal(), SphereShader::TextureCoordinates());
    CgResourceManager::instance().set("sphere",meshS, ResourceDataState::Final, ResourcePolicy::Manual);
    CgResourceManager::instance().set("sphere_indices", indexBufferS, ResourceDataState::Final, ResourcePolicy::Manual);
    CgResourceManager::instance().set("sphere_vertices", vertexBufferS, ResourceDataState::Final, ResourcePolicy::Manual);
}

MAGNUM_APPLICATION_MAIN(MyApplication)
