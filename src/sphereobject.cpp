#include "sphereobject.h"

// Algorithms
#include <Magnum/Math/Algorithms/GramSchmidt.h>

SphereObject::SphereObject(string n, const ResourceKey meshKey, const ResourceKey shaderKey, Vector3 vel, Color3 col, Object3D* parent, SceneGraph::DrawableGroup3D* group)
    : SceneObject(n,meshKey,vel,col,parent,group),
      shader(CgResourceManager::instance().get<SphereShader>(shaderKey))
{
    shape = NULL;
    hasTexture = false;
}

void SphereObject::update(Float duration)
{
    translate(velocity*duration);
}

void SphereObject::setShader(const ResourceKey shaderKey)
{
    shader = CgResourceManager::instance().get<SphereShader>(shaderKey);
}

void SphereObject::setShape(Shapes::AbstractShape3D *sh)
{
    shape = (Shapes::Shape<Shapes::Sphere3D>*)sh;
}

Shapes::AbstractShape3D* SphereObject::getShape()
{
    return shape;
}

void SphereObject::setTexture(Texture2D *tex)
{
    texture = tex;
}

void SphereObject::setTextured(bool isTex)
{
    hasTexture = isTex;
}

bool SphereObject::isTextured()
{
    return hasTexture;
}

void SphereObject::draw(const Matrix4& transformationMatrix, SceneGraph::AbstractCamera3D& camera)
{
    shader->setTransformationMatrix(transformationMatrix)
            .setColor(color)
            .setTexture(*texture)
            .setTextured(hasTexture)
            .setProjectionMatrix(camera.projectionMatrix())
            .setNormalMatrix(Math::Algorithms::gramSchmidtOrthonormalize(transformationMatrix.rotationScaling()))
            .use();
    mesh->draw(*shader);
}
