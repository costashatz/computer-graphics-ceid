#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

// Typedefs
#include "typedefs.h"

// Color
#include <Magnum/Color.h>

// Sphere Shape
#include <Magnum/Shapes/AbstractShape.h>

using namespace Magnum;

// Abstract class: base for Sphere and Simple Objects
class SceneObject: public Object3D, SceneGraph::Drawable3D
{
public:
    SceneObject(string n, const ResourceKey meshKey, Vector3 vel, Color3 col, Object3D* parent, SceneGraph::DrawableGroup3D* group);

    string Name();

    virtual void update(Float duration) = 0;
    virtual void setShader(const ResourceKey shaderKey) = 0;

    virtual void setShape(Shapes::AbstractShape3D* sh) = 0;
    virtual Shapes::AbstractShape3D* getShape() = 0;

    void setVelocity(const Vector3& vel);
    Vector3 getVelocity();

protected:
    // Member variables
    Resource<Mesh> mesh;
    string name;
    Vector3 velocity;
    Color3 color;

};


#endif // SCENEOBJECT_H
