#include "sceneobject.h"

// Algorithms
#include <Magnum/Math/Algorithms/GramSchmidt.h>

SceneObject::SceneObject(string n, const ResourceKey meshKey, Vector3 vel, Color3 col, Object3D* parent, SceneGraph::DrawableGroup3D* group)
    : Object3D(parent), SceneGraph::Drawable3D(*this, group),
      mesh(CgResourceManager::instance().get<Mesh>(meshKey)),
      name(n),
      velocity(vel),
      color(col)
{
}

string SceneObject::Name()
{
    return name;
}

void SceneObject::setVelocity(const Vector3 &vel)
{
    velocity = vel;
}

Vector3 SceneObject::getVelocity()
{
    return velocity;
}

