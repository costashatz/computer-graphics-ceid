#ifndef SPHERESHADER_H
#define SPHERESHADER_H

#include <Magnum/AbstractShaderProgram.h>
#include <Magnum/Color.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/Matrix3.h>
#include <Magnum/Math/Vector3.h>
#include <Magnum/Math/Vector4.h>
#include <Magnum/Texture.h>
#include "light.h"
#include <vector>
#include <string>
#include <memory>


using namespace Magnum;
using namespace std;

class SphereShader : public AbstractShaderProgram
{
public:
    typedef Attribute<0, Vector3> Position;
    typedef Attribute<1, Vector3> Normal;
    typedef Attribute<2, Vector2> TextureCoordinates;

    explicit SphereShader();

    // Helper functions to set Uniform values

    SphereShader& setTransformationMatrix(const Matrix4& transformationMatrix)
    {
        setUniform(transformation,transformationMatrix);
        return *this;
    }

    SphereShader& setProjectionMatrix(const Matrix4& projectionMatrix)
    {
        setUniform(projection,projectionMatrix);
        return *this;
    }

    SphereShader& setViewMatrix(const Matrix4& viewMatrix)
    {
        setUniform(view,viewMatrix);
        return *this;
    }

    SphereShader& setColor(const Color3& col)
    {
        setUniform(color,col);
        return *this;
    }

    SphereShader& setTexture(Texture2D& texture)
    {
        texture.bind(TextureLayer);
        return *this;
    }

    SphereShader& setTextured(bool& textured)
    {
        setUniform(textureEnable,textured);
        return *this;
    }

    SphereShader& setNormalMatrix(const Matrix3& normalMatrix)
    {
        setUniform(normalMat,normalMatrix);
        return *this;
    }

    SphereShader& setNumberOfLights(const Int& number)
    {
        setUniform(numberOfLights,number);
        return *this;
    }

    SphereShader& setLights(const vector<std::unique_ptr<Light> >& Lights)
    {
        for(unsigned int i=0;i<Lights.size();i++)
        {
            Int p = uniformLocation("lights["+std::to_string(i)+"].position");
            Int d = uniformLocation("lights["+std::to_string(i)+"].diffuse");
            Int s = uniformLocation("lights["+std::to_string(i)+"].specular");
            Int ca = uniformLocation("lights["+std::to_string(i)+"].constantAttenuation");
            Int la = uniformLocation("lights["+std::to_string(i)+"].linearAttenuation");
            Int qa = uniformLocation("lights["+std::to_string(i)+"].quadraticAttenuation");
            Int sc = uniformLocation("lights["+std::to_string(i)+"].spotCutoff");
            Int se = uniformLocation("lights["+std::to_string(i)+"].spotExponent");
            Int sd = uniformLocation("lights["+std::to_string(i)+"].spotDirection");

            Vector3 k=Lights[i]->transformationMatrix().translation();
            Vector4 v{k.x(),k.y(),k.z(),Lights[i]->position.w()};
            setUniform(p,v);
            setUniform(d,Lights[i]->diffuse);
            setUniform(s,Lights[i]->specular);
            setUniform(ca,Lights[i]->constantAttenuation);
            setUniform(la,Lights[i]->linearAttenuation);
            setUniform(qa,Lights[i]->quadraticAttenuation);
            setUniform(sc,Lights[i]->spotCutoff);
            setUniform(se,Lights[i]->spotExponent);
            setUniform(sd,Lights[i]->spotDirection);
        }
        return *this;
    }

private:
    // Texture Layer enumeration (we only have one)
    enum: Int { TextureLayer = 0 };

    // Uniform handlers
    Int transformation;
    Int projection;
    Int color;
    Int normalMat;
    Int numberOfLights;
    Int view;
    Int textureEnable;
};

#endif // SPHERESHADER_H
