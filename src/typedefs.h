#ifndef TYPEDEFS_H
#define TYPEDEFS_H

// Magnum Required
#include <Magnum/DefaultFramebuffer.h>

// Mesh
#include <Magnum/Mesh.h>

// Resource Manager
#include <Magnum/ResourceManager.h>

// Scene Graph
#include <Magnum/SceneGraph/Camera3D.h>
#include <Magnum/SceneGraph/Drawable.h>
#include <Magnum/SceneGraph/MatrixTransformation3D.h>
#include <Magnum/SceneGraph/Scene.h>

// Custom Shader
#include "customshader.h"
#include "sphereshader.h"

using namespace Magnum;
using namespace std;

// General typedefs and statements
typedef SceneGraph::Scene<SceneGraph::MatrixTransformation3D> Scene3D;
typedef SceneGraph::Object<SceneGraph::MatrixTransformation3D> Object3D;
typedef ResourceManager<Buffer, Mesh, CustomShader, SphereShader> CgResourceManager;

#endif // TYPEDEFS_H
