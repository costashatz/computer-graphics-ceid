#include "customshader.h"

// Corrade
#include <Corrade/Utility/Resource.h>
#include <Corrade/Utility/Assert.h>

// Magnum
#include <Magnum/Context.h>
#include <Magnum/Shader.h>
#include <Magnum/Version.h>

CustomShader::CustomShader()
{
    MAGNUM_ASSERT_VERSION_SUPPORTED(Version::GL330);

    // Get resource
    Utility::Resource rs("data");

    // Compile vertex shader
    Shader vert(Version::GL330, Shader::Type::Vertex);
    vert.addSource(rs.get("CustomShader.vert"));
    CORRADE_INTERNAL_ASSERT_OUTPUT(vert.compile());
    attachShader(vert);

    // Compile fragment shader
    Shader frag(Version::GL330, Shader::Type::Fragment);
    frag.addSource(rs.get("CustomShader.frag"));
    CORRADE_INTERNAL_ASSERT_OUTPUT(frag.compile());
    attachShader(frag);

    // Link shaders
    CORRADE_INTERNAL_ASSERT_OUTPUT(link());

    // Get uniform handlers
    transformation = uniformLocation("transformation");
    projection = uniformLocation("projection");
    normalMat = uniformLocation("normalMatrix");
    numberOfLights = uniformLocation("numberOfLights");
    view = uniformLocation("viewMatrix");
}

