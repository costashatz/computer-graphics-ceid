#ifndef SIMPLEOBJECT_H
#define SIMPLEOBJECT_H

// Typedefs
#include "typedefs.h"

// Sphere Shape
#include <Magnum/Shapes/AxisAlignedBox.h>
#include <Magnum/Shapes/Shape.h>

// SceneObject
#include "sceneobject.h"

using namespace Magnum;

class SimpleObject: public SceneObject
{
public:
    SimpleObject(string n, const ResourceKey meshKey, const ResourceKey shaderKey, Object3D* parent, SceneGraph::DrawableGroup3D* group);

    void update(Float duration);
    void setShader(const ResourceKey shaderKey);

    void setShape(Shapes::AbstractShape3D* sh);
    Shapes::AbstractShape3D* getShape();

private:
    // Member variables
    Resource<CustomShader> shader;
    Shapes::Shape<Shapes::AxisAlignedBox3D>* shape;

    void draw(const Matrix4& transformationMatrix, SceneGraph::AbstractCamera3D& camera) override;
};

#endif // SIMPLEOBJECT_H
