#ifndef CUSTOMSHADER_H
#define CUSTOMSHADER_H

#include <Magnum/AbstractShaderProgram.h>
#include <Magnum/Math/Matrix4.h>
#include <Magnum/Math/Matrix3.h>
#include <Magnum/Math/Vector3.h>
#include <Magnum/Math/Vector4.h>
#include "light.h"
#include <vector>
#include <string>
#include <memory>

using namespace Magnum;
using namespace std;

class CustomShader : public AbstractShaderProgram
{
public:
    typedef Attribute<0, Vector3> Position;
    typedef Attribute<1, Vector3> Color;
    typedef Attribute<2, Vector3> Normal;

    explicit CustomShader();

    // Helper functions to set Uniform values

    CustomShader& setTransformationMatrix(const Matrix4& transformationMatrix)
    {
        setUniform(transformation,transformationMatrix);
        return *this;
    }

    CustomShader& setProjectionMatrix(const Matrix4& projectionMatrix)
    {
        setUniform(projection,projectionMatrix);
        return *this;
    }

    CustomShader& setViewMatrix(const Matrix4& viewMatrix)
    {
        setUniform(view,viewMatrix);
        return *this;
    }

    CustomShader& setNormalMatrix(const Matrix3& normalMatrix)
    {
        setUniform(normalMat,normalMatrix);
        return *this;
    }

    CustomShader& setNumberOfLights(const Int& number)
    {
        setUniform(numberOfLights,number);
        return *this;
    }

    CustomShader& setLights(const vector<std::unique_ptr<Light> >& Lights)
    {
        for(unsigned int i=0;i<Lights.size();i++)
        {
            Int p = uniformLocation("lights["+std::to_string(i)+"].position");
            Int d = uniformLocation("lights["+std::to_string(i)+"].diffuse");
            Int s = uniformLocation("lights["+std::to_string(i)+"].specular");
            Int ca = uniformLocation("lights["+std::to_string(i)+"].constantAttenuation");
            Int la = uniformLocation("lights["+std::to_string(i)+"].linearAttenuation");
            Int qa = uniformLocation("lights["+std::to_string(i)+"].quadraticAttenuation");
            Int sc = uniformLocation("lights["+std::to_string(i)+"].spotCutoff");
            Int se = uniformLocation("lights["+std::to_string(i)+"].spotExponent");
            Int sd = uniformLocation("lights["+std::to_string(i)+"].spotDirection");

            Vector3 k=Lights[i]->transformationMatrix().translation();
            Vector4 v{k.x(),k.y(),k.z(),Lights[i]->position.w()};
            setUniform(p,v);
            setUniform(d,Lights[i]->diffuse);
            setUniform(s,Lights[i]->specular);
            setUniform(ca,Lights[i]->constantAttenuation);
            setUniform(la,Lights[i]->linearAttenuation);
            setUniform(qa,Lights[i]->quadraticAttenuation);
            setUniform(sc,Lights[i]->spotCutoff);
            setUniform(se,Lights[i]->spotExponent);
            setUniform(sd,Lights[i]->spotDirection);
        }
        return *this;
    }

private:
    // Uniform handlers
    Int transformation;
    Int projection;
    Int normalMat;
    Int numberOfLights;
    Int view;
};

#endif // CUSTOMSHADER_H
