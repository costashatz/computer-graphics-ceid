#ifndef LIGHT_H
#define LIGHT_H

// Magnum Math
#include <Magnum/Math/Vector3.h>
#include <Magnum/Math/Vector4.h>
#include <Magnum/Color.h>

// Scene Graph
#include <Magnum/SceneGraph/MatrixTransformation3D.h>

using namespace Magnum;

typedef SceneGraph::Object<SceneGraph::MatrixTransformation3D> Object3D;

class Light : public Object3D
{
public:
    Light(Object3D* parent, Vector4 pos, Color4 diff, Color4 spec, Float cAtt, Float lAtt, Float qAtt, Float spCutoff, Float spotExp, Vector3 spotDir);

    // Member variables
    Vector4 position;
    Color4 diffuse;
    Color4 specular;
    Float constantAttenuation, linearAttenuation, quadraticAttenuation;
    Float spotCutoff, spotExponent;
    Vector3 spotDirection;
};

#endif // LIGHT_H
