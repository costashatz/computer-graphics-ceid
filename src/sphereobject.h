#ifndef SPHEREOBJECT_H
#define SPHEREOBJECT_H

// Typedefs
#include "typedefs.h"

// Color
#include <Magnum/Color.h>

// Sphere Shape
#include <Magnum/Shapes/Sphere.h>
#include <Magnum/Shapes/Shape.h>

// Texture
#include <Magnum/Texture.h>

// SceneObject
#include "sceneobject.h"

using namespace Magnum;

class SphereObject: public SceneObject
{
public:
    SphereObject(string n, const ResourceKey meshKey, const ResourceKey shaderKey, Vector3 vel, Color3 col, Object3D* parent, SceneGraph::DrawableGroup3D* group);

    void update(Float duration);
    void setShader(const ResourceKey shaderKey);

    void setShape(Shapes::AbstractShape3D* sh);
    Shapes::AbstractShape3D* getShape();

    void setTexture(Texture2D* tex);
    void setTextured(bool isTex);
    bool isTextured();

private:
    // Member variables
    Resource<SphereShader> shader;
    Shapes::Shape<Shapes::Sphere3D>* shape;
    Texture2D* texture;
    bool hasTexture;

    void draw(const Matrix4& transformationMatrix, SceneGraph::AbstractCamera3D& camera) override;
};

#endif // SPHEREOBJECT_H
